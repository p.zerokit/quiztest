import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';
@Component({
  selector: 'app-adminpage',
  templateUrl: './adminpage.component.html',
  styleUrls: ['./adminpage.component.scss'],
})
export class AdminpageComponent implements OnInit {
  stores: any;
  store: any;
  Listbooking: any;
  lookFor: string = 'All';

  constructor(private dataservice: DataServiceService) {}

  ngOnInit(): void {
    this.stores = this.dataservice.getData();
  }

  today(time: number) {
    const someDate = new Date(time);
    const today = new Date();
    return (
      someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
    );
  }
  selectWhere(st: any, w: string) {
    this.store = st;

    if (this.store) {
      const booking = this.dataservice.booking;
      const data = booking.filter((d: any) => {
        return this.lookFor == 'All'
          ? d.name == st.name && ( d.customer != ''
            ? true
            : Date.now() - d.date <= 60000 )
          : d.name == st.name && (d.customer != ''
          ? true
          : Date.now() - d.date <= 60000 )&& this.today(d.date);
      });
      this.Listbooking = data;
    }
  }
}
