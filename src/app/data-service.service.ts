import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DataServiceService {
  category = ['อาหารไทย', 'อาหารตามสั่ง', 'อาหารญี่ปุ่น'];

  booking: any = [
    {
      id: 0,
      name: 'ร้านป้าแดงตามสั่ง',
      customer: 'ธนาธร',
      date: 1626544690424,
      table: [
        {
          byunit: 4,
          select: 1,
          title: 'โต๊ะเล็ก',
          unit: 4,
        },
      ],
      timeBokking: 10,
    },
    {
      id: 1,
      name: 'ร้านป้าแดงตามสั่ง',
      customer: '',
      date: 1626544690424,
      table: [
        {
          byunit: 4,
          select: 1,
          title: 'โต๊ะเล็ก',
          unit: 4,
        },
      ],
      timeBokking: 11,
    },
  ];

  getData() {
    const data = [
      {
        name: 'ร้านป้าแดงตามสั่ง',
        menu: [
          'ข้าวกระเพาไก่ไข่ดาว',
          'ข้าวผัดกุ้ง',
          'ข้าวผัดทะเลรวม',
          'กระเพราหมูสับ ราดข้าว',
          'กระเพราหมูชิ้น ราดข้าว',
        ],
        openTime: 10.0,
        closeTime: 19.0,
        category: ['อาหารไทย','อาหารตามสั่ง'],
        table: [
          {
            title: 'โต๊ะเล็ก',
            unit: 4,
            byunit: 4,
          },
          {
            title: 'โต๊ะกลาง',
            unit: 2,
            byunit: 6,
          },
          {
            title: 'โต๊ะใหญ่',
            unit: 1,
            byunit: 8,
          },
        ],
      },
      {
        name: 'ร้านป้าข้างโรงเรียน',
        menu: ['กระเพาหมูกรอบ', 'สุกกี้แห้งทะเล', 'สุกกี้แห้งกุ้ง'],
        openTime: 10.0,
        closeTime: 19.0,
        category: ['อาหารไทย'],
        table: [
          {
            title: 'โต๊ะเล็ก',
            unit: 15,
            byunit: 4,
          },
          {
            title: 'โต๊ะกลาง',
            unit: 8,
            byunit: 6,
          },
          {
            title: 'โต๊ะใหญ่',
            unit: 2,
            byunit: 8,
          },
        ],
      },
      {
        name: 'ซูชิวังหลัง',
        menu: ['ทาโกยากิ', 'กุ้งดอง', 'สาหร่ายญี่ปุ่นสีเขียว', 'ปลาซาบะย่าง '],
        openTime: 10.0,
        closeTime: 19.0,
        category: ['อาหารญี่ปุ่น'],
        table: [
          {
            title: 'โต๊ะเล็ก',
            unit: 50,
            byunit: 4,
          },
          {
            title: 'โต๊ะกลาง',
            unit: 2,
            byunit: 6,
          },
          {
            title: 'โต๊ะใหญ่',
            unit: 1,
            byunit: 8,
          },
        ],
      },
      {
        name: 'ชาบูแมว',
        menu: ['ทาโกะยากิ ซุปเปอร์ชีส', 'กระเพราหมูสับปลาหมึกคัพ (cup) ', 'ชาบูแมวดำ', 'ไข่ดาว'],
        openTime: 10.0,
        closeTime: 19.0,
        category: ['อาหารญี่ปุ่น', 'อาหารไทย'],
        table: [
          {
            title: 'โต๊ะเล็ก',
            unit: 12,
            byunit: 4,
          },
          {
            title: 'โต๊ะกลาง',
            unit: 5,
            byunit: 6,
          },
          {
            title: 'โต๊ะใหญ่',
            unit: 3,
            byunit: 8,
          },
        ],
      },
    ];
    return data;
  }
}
