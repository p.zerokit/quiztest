import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
})
export class HomepageComponent implements OnInit {
  name: string = '';
  listData: any;
  listDataByCat: any;
  category: any;
  toBooking: any = null;
  timeBooking: Number = 0;
  bookingTable: any = [];
  onSelected: number = 0;
  indexBook: number = 0;
  nameCustumer: string = '';
  success: boolean = false;
  myBooking: any = {};
  constructor(private dataservice: DataServiceService) {}

  ngOnInit(): void {
    this.category = this.dataservice.category;
  }

  dataGet() {
    this.listData = [];
    this.listDataByCat = [];
    if (this.name != '') {
      this.listData = this.dataservice
        .getData()
        .map((d: any) => {
          const menu = d.menu;
          d.menu = menu.filter((m: any) => m.indexOf(this.name) >= 0);
          return d;
        })
        .filter((re) => re.menu.length > 0);
    } else {
      this.listData = [];
    }
  }

  getByCategory(title: string) {
    this.listData = [];
    this.listDataByCat = [];
    this.listDataByCat = this.dataservice
      .getData()
      .filter((data) => data.category!.indexOf(title) >= 0);
  }

  opentoBooking(store: any) {
    this.toBooking = store;
    this.timeBooking = store.openTime;
    this.timeChanged();
  }

  timeChanged() {
    var table: any = {};


    var booking = this.dataservice.booking;
    booking.forEach(
      (st: {
        name: any;
        timeBokking: number;
        table: any[];
        customer: string;
        date: number;
      }) => {


        if (
          st.name == this.toBooking.name &&
          st.timeBokking == this.timeBooking &&
          (st.customer == '' ? Date.now() - st.date <= 60000 : true)
        ) {
          st.table.forEach((element: any) => {
            if (table[element.title] != null) {
              table[element.title] += element.select;
            } else {
              table[element.title] = element.select;
            }
          });
        }
      }
    );



    this.bookingTable = [];
    this.toBooking.table?.forEach((element: any) => {
      this.bookingTable.push({
        title: element.title,
        unit:
          element.unit -
          (table[element.title] == null ? 0 : table[element.title]),
        byunit: element.byunit,
        select: 0,
      });
    });
  }

  selectChanged() {
    var i = 0;
    this.bookingTable.forEach((element: any) => {
      i += element.select;
    });
    this.onSelected = i;
  }

  bookingUp() {
    var table: any[] = [];
    this.bookingTable.forEach((element: any) => {
      if (element.select > 0) {
        table.push(element);
      }
    });
    this.indexBook = this.dataservice.booking.length;
    var data = {
      id: this.indexBook,
      name: this.toBooking.name,
      customer: '',
      date: Date.now(),
      table: table,
      timeBokking: this.timeBooking,
    };
    this.dataservice.booking.push(data);
 
  }

  inputName() {
    if (Date.now() - this.dataservice.booking[this.indexBook].date <= 60000) {
      this.success = true;
      this.dataservice.booking[this.indexBook].customer = this.nameCustumer;
      this.myBooking = this.dataservice.booking[this.indexBook];
      this.reData();
    } else {
      alert('Time Out !!');
      this.reData();
    }
  }

  reData() {
    this.name = '';
    this.listData = [];
    this.listDataByCat = [];
    this.toBooking = null;
    this.timeBooking = 0;
    this.bookingTable = [];
    this.onSelected = 0;
    this.indexBook = 0;
    this.nameCustumer = '';
  }
}
